require './canvas'
require './point'

@canvas = nil

File.open('input.txt', 'r') do |f|
  f.each_line do |line|
    line_values = line.split(' ')
    next if line_values.count.zero?

    case line_values.first.upcase
    when 'C'
      @canvas = Canvas.new(width: line_values[1].to_i, height: line_values[2].to_i)
    when 'L'
      @canvas&.draw_line!(*Point.two_points(line_values))
    when 'R'
      @canvas&.draw_rectangle!(*Point.two_points(line_values))
    when 'B'
      point = Point.new(x: line_values[1].to_i, y: line_values[2].to_i)
      @canvas&.bucket_fill!(point, line_values[3])
    end
    @canvas&.draw
  end
end
