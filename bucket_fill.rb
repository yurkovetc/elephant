require './point'

module BucketFill
  def bucket_fill!(point, color)
    return unless point.in_canvas?(@area[0].size, @area.size)
    @old_color = @area[point.y][point.x]
    fill_neighbor(point.x, point.y, color)
  end

  private

  def fill_neighbor(x, y, color)
    return if @area[y][x] != @old_color
    @area[y][x] = color
    fill_neighbor(x, y + 1, color)
    fill_neighbor(x, y - 1, color)
    fill_neighbor(x + 1, y, color)
    fill_neighbor(x - 1, y, color)
  end
end
