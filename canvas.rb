require './line'
require './rectangle'
require './bucket_fill'

class Canvas
  include Line
  include Rectangle
  include BucketFill

  BORDER_X_SYMBOL = '-'.freeze
  BORDER_Y_SYMBOL = '|'.freeze

  def initialize(args)
    @width = args[:width]
    @height = args[:height]
    @area = []
    (@height + 2).times do
      @area.push(Array.new(@width + 2, ' '))
    end
    draw_border
  end

  def draw
    (@height + 2).times do |y|
      0.upto(@width + 1).each do |x|
        print @area[y][x]
      end
      puts
    end
    puts
  end

  private

  def draw_border
    draw_border_width
    draw_border_height
  end

  def draw_border_width
    [0, @height + 1].compact.each do |y|
      (@width + 2).times do |x|
        @area[y][x] = BORDER_X_SYMBOL
      end
    end
  end

  def draw_border_height
    [0, @width + 1].compact.each do |x|
      @height.times do |y|
        @area[y + 1][x] = BORDER_Y_SYMBOL
      end
    end
  end
end
