class Point
  attr_accessor :x, :y

  def initialize(args)
    @x = args[:x]
    @y = args[:y]
  end

  def in_canvas?(canvas_x, canvas_y)
    return unless @x.positive? && @y.positive?
    canvas_x - 2 >= @x && canvas_y - 2 >= @y
  end

  def self.two_points(values)
    values.map!(&:to_i)
    point1 = Point.new(x: values[1], y: values[2])
    point2 = Point.new(x: values[3], y: values[4])
    [point1, point2]
  end
end
