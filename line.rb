require './point'

module Line
  LINE_SYMBOL = 'x'.freeze

  def draw_line!(point1, point2)
    [point1, point2].each { |p| return unless p.in_canvas?(@area[0].size, @area.size) }
    if point1.x == point2.x && point1.y != point2.y
      interval(point1.y, point2.y).each do |y|
        @area[y][point1.x] = LINE_SYMBOL
      end
    elsif point1.x != point2.x && point1.y == point2.y
      interval(point1.x, point2.x).each do |x|
        @area[point1.y][x] = LINE_SYMBOL
      end
    end
  end

  private

  def interval(a, b)
    a > b ? a.downto(b) : a.upto(b)
  end
end
