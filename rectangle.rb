require './point'

module Rectangle
  RECTANGE_SYMBOL = 'x'.freeze

  def draw_rectangle!(point1, point2)
    [point1, point2].each { |p| return unless p.in_canvas?(@area[0].size, @area.size) }
    if point1.x > point2.x && point1.y > point2.y
      assign_rectangle(point2, point1)
    elsif point1.x < point2.x && point1.y < point2.y
      assign_rectangle(point1, point2)
    end
  end

  private

  def assign_rectangle(point1, point2)
    rect_width(point1, point2)
    rect_heigth(point1, point2)
  end

  def rect_width(point1, point2)
    [point1.y, point2.y].compact.each do |y|
      (point1.x..point2.x).each do |x|
        @area[y][x] = RECTANGE_SYMBOL
      end
    end
  end

  def rect_heigth(point1, point2)
    [point1.x, point2.x].compact.each do |x|
      (point1.y..point2.y).each do |y|
        @area[y][x] = RECTANGE_SYMBOL
      end
    end
  end
end
